SELECT prcp,min,max,temp,date,mo,year,da
FROM
`bigquery-public-data.noaa_gsod.gsod2020` t
WHERE 
t.date >= '2019-01-27'
AND t.date <= '2020-12-20'
